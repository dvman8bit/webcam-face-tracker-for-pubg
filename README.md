
Face Tracking Software fot bends in PUBG and other games.

Управляй наклонами в игре с помощью своей вебкамеры.

Based on sources from:
OpenCV-Face-andmore-Tracker
https://github.com/A9T9/OpenCV-Face-andmore-Tracker

===========================


OpenCV based face (and eye, nose, mouth) detection example application. The tutorial is provided as ready to compile/run Windows Visual Studio project (C#, .NET, EmguCV). Just download, compile and have fun with it. Or use the provided binary setup and play webcam face tracking right away.
