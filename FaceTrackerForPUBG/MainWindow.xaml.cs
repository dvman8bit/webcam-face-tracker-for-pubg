﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using FaceFinderDemo.Camera;
using FaceFinderDemo.FaceDetection;
using FaceFinderDemo.ImageProcessing;
using Microsoft.Win32;
using Point = System.Drawing.Point;
using System.Windows.Forms;


namespace FaceFinderDemo
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        FaceDetectorDevice faceDetection;
        MainWindowViewModel model;
        CameraDevice camera;
        ImageDevice image;

        public MainWindow()
        {

            model = new MainWindowViewModel();
            DataContext = model;

            // Connect the image processor chain
            faceDetection = new FaceDetectorDevice();
            camera = new CameraDevice();
            image = new ImageDevice();
            faceDetection.ImageAvailable += ImageAvailable;
            faceDetection.FaceDetectorStateChanged += FaceDetectorStateChanged;
            model.SelectedDetectionMode = FaceDetectorDevice.DetectionModes.Periodic;
            model.DrawDetection = true;
            model.DetectionPeriod = 40;
            model.LastDetection = "None";

            InitializeComponent();
            SizeToContent = SizeToContent.WidthAndHeight;

            Loaded += OnLoaded;
            Closed += OnClosed;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            model.AvailableCameras = DeviceEnumerator.GetDeviceNames();
            SelectedCamera.SelectedIndex = 0;
        }

        void OnClosed(object sender, EventArgs eventArgs)
        {
            camera.StopCamera();
            image.StopSending();
        }

        void ImageAvailable(object sender, ImageAvailableEventArgs e)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                if (model.IsCapturing)
                {
                    DetectedImage.Source = EmguWpfHelper.ToBitmapSource(e.Image);    
                }
                else
                {
                    DetectedImage.Source = new BitmapImage(new Uri(@"/Resources/camera_image_placeholder.png", UriKind.Relative));
                }
            }));
        }

        char keypressed = ' ';
        FaceFeatures lastFace;

        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);
        void FaceDetectorStateChanged(object sender, FaceDetectionEventArgs e)
        {
            StringBuilder message = new StringBuilder();
            model.CurrentlyDetecting = e.Starting;
            
            if (!e.Starting)
            { 
                if (e.Faces.Count == 0 && lastFace != null)
                {
                    e.Faces.Add(lastFace);

                }
                if (e.Faces.Count == 0 )
                {
                    model.LastDetection = "None";
                    message.AppendFormat("no face found");
                }
                else if (e.Faces.Count == 1)
                {

                        lastFace = e.Faces[0];
    


                    //center
                    if (e.Faces[0].FaceLocation.X > model.IeX+20 && e.Faces[0].FaceLocation.X < model.IqX-20)
                    {
                        if (keypressed != ' ')
                        {
                            keybd_event((byte)keypressed, 0, 0x0002, 0);
                            model.LastDetection = "not bending";
                            keypressed = ' ';
                        }
                    }
                    else
                    {
                        if(e.Faces[0].FaceLocation.X < model.IeX)
                        {
                            if (keypressed != model.RightKey[0])
                            {
                                keybd_event((byte)model.LeftKey[0], 0, 0x0002, 0);
                                keybd_event((byte)model.RightKey[0], 0, 0x0001, 0);
                                keypressed = model.RightKey[0];
                                model.LastDetection = "Bend right -> ("+ model.RightKey[0] + ")";
                            }
                        } else
                        {
                            if (keypressed != model.LeftKey[0] && e.Faces[0].FaceLocation.X > model.IqX)
                            {
                                keybd_event((byte)model.RightKey[0], 0, 0x0002, 0);
                                keybd_event((byte)model.LeftKey[0], 0, 0x0001, 0);
                                keypressed = model.LeftKey[0];
                                model.LastDetection = "Bend left <- ("+ model.LeftKey[0] + ")";
                            }
                        }
                    }
                    LogMessage(message.ToString());
                }
            }  
        }

        void LogMessage(string message)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                if(LogBox.LineCount > 100)
                {
                    LogBox.Text = "";
                }
                LogBox.AppendText(message + "\n");
                LogBox.ScrollToEnd();
            }));
        }

        void Cameras_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (camera.IsCapturing)
            {
                LogMessage("Camera changed");
                camera.StopCamera();
                camera.StartCamera(SelectedCamera.SelectedIndex);
            }
        }

        void StopCapturing_OnClick(object sender, RoutedEventArgs e)
        {
            LogMessage("Capturing stopped");
            model.IsCapturing = false;
            camera.StopCamera();
            image.StopSending();
            Task.Factory.StartNew(() =>
            {
                Thread.Sleep(1000);
                Dispatcher.Invoke(new Action(() =>
                {
                    DetectedImage.Source = new BitmapImage(new Uri(@"/Resources/camera_image_placeholder.png", UriKind.Relative));
                }));
            });
        }

        void StartCapturing_OnClick(object sender, RoutedEventArgs e)
        {
            if (model.AvailableCameras.Count == 0)
            {
                System.Windows.MessageBox.Show("No camera available!");
                return;
            }

            if (SelectedCamera.SelectedIndex < 0)
            {
                System.Windows.MessageBox.Show("No camera selected!");
                return;
            }
            LogMessage("Capturing started from camera");
            model.IsCapturing = true;
            faceDetection.DetectionMode = FaceDetectorDevice.DetectionModes.AllFrames;
            faceDetection.DrawDetection = true;
            faceDetection.AttachSource(camera);
            faceDetection.ResetDetections();
            camera.StartCamera(SelectedCamera.SelectedIndex);
        }

        private void RightKey_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            RightKey.Text = e.Key.ToString();
        }
        private void RightKey_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            RightKey.Text = "";
        }

        private void LeftKey_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            LeftKey.Text = e.Key.ToString();
        }
        private void LeftKey_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            LeftKey.Text = "";
        }

        private void Label_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.youtube.com/КакойтоАйтишник");
        }
    }
}
